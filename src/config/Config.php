<?php

namespace promoter\config;


/**
 * 配置类
 * Class Config
 * @package ucenter\config
 */
class Config
{
    /**
     * 配置列表
     * @var array
     */
    public static $config = [
        // 协议+域名
        'doMain' => '',
        // 签名key
        'signKey' => '',
    ];

    /**
     * 获取配置
     * @author PengShuHai<pengshuhai@jybdshop.cn>
     * @date 2019/2/1 16:04:49
     * @param string $configName
     * @return mixed|null
     */
    public static function get($configName)
    {
        $config = null;
        if (function_exists('getPromoterConfig')) {
            $config = getPromoterConfig($configName);
        }

        if (is_null($config)) {
            if (isset(self::$config[$configName])) {
                $config = self::$config[$configName];
            }
        }
        return $config;
    }

}