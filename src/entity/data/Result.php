<?php
namespace promoter\entity\data;

use promoter\api\Api;

/**
 * 内部返回值 复杂数据实体
 * Class Result
 * @package core\entity\data
 */
class Result
{
    /**
     * 执行是否正确
     * @var bool
     */
    public $state = true;

    /**
     * 错误码(仅失败才有)
     * @var string
     */
    public $errorCode = '';

    /**
     * 错误/成功消息内容
     * @var string
     */
    public $message = '';

    /**
     * 数据内容(仅成功才有)
     * @var mixed
     */
    public $result = null;

    /**
     * 拓展数组(附加信息使用)
     * @var array
     */
    protected $extends = [];


    /**
     * set
     * @author PengShuHai<pengshuhai@jybdshop.cn>
     * @date 2019/1/17 11:12:56
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->extends[$name] = $value;
    }

    /**
     * get
     * @author PengShuHai<pengshuhai@jybdshop.cn>
     * @date 2019/1/17 11:13:29
     * @param $name
     * @return mixed|null
     */
    public function __get($name)
    {
        if (isset($this->extends[$name])) {
            return $this->extends[$name];
        } else {
            return null;
        }
    }

    /**
     * 获取完整拓展数组
     * @author PengShuHai<pengshuhai@jybdshop.cn>
     * @date 2019/1/17 12:55:51
     * @return array
     */
    public function getExtends()
    {
        return $this->extends;
    }

    /**
     * 返回结果数组
     * @author PengShuHai<pengshuhai@jybdshop.cn>
     * @date 2019/2/1 15:42:19
     * @return array
     */
    public function toCallback()
    {
        if ($this->state) {
            return array('state' => Api::CODE_API_SUCCESS, 'msg' => $this->message, 'data' => $this->result);
        } else {
            return array('state' => $this->errorCode, 'msg' => $this->message, 'data' => $this->result);
        }
    }

    /**
     * 创建成功实体
     * @author PengShuHai<pengshuhai@jybdshop.cn>
     * @date 2019/1/17 11:23:43
     * @param mixed $result   数据内容
     * @param string $message 结果信息
     * @param array $extends  附加参数
     * @return Result
     */
    public static function Success($result = null, $message = '', $extends = [])
    {
        $resultObj = new static();
        $resultObj->state = true;
        $resultObj->result = $result;
        $resultObj->message = $message;
        $resultObj->extends = $extends;
        return $resultObj;
    }

    /**
     * 创建失败实体
     * @author PengShuHai<pengshuhai@jybdshop.cn>
     * @date 2019/1/17 11:26:50
     * @param string $message   错误信息
     * @param string $errorCode 错误状态码
     * @param array $extends    附加参数
     * @return Result
     */
    public static function Error($message = '', $errorCode = '000', $extends = []) 
    {
        $resultObj = new static();
        $resultObj->state = false;
        $resultObj->message = $message;
        $resultObj->errorCode = $errorCode;
        $resultObj->extends = $extends;
        return $resultObj;
    }

}