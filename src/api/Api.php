<?php

namespace promoter\api;

use promoter\config\Config;
use promoter\curl\HttpCurl;
use promoter\curl\HttpCurlResponse;
use promoter\entity\data\Result;
use promoter\helpers\StringHelpers;

class Api
{
    /**
     * api接口正确code
     * @var string
     */
     const CODE_API_SUCCESS = '200';

    /**
     * api接口内部异常
     * @var string
     */
     const CODE_API_ERROR = '000';

    /**
     * api业务错误
     */
     const CODE_API_BUSINESS_ERROR = '001';

    /**
     * api验签失败
     */
     const CODE_API_SIGN_ERROR = '002';

    /**
     * api服务器错误
     */
     const CODE_API_SERVER_ERROR = 'api_001';

    /**
     * sdk端异常code
     * @var string
     */
    const CODE_SDK_EXCEPTION = 'sdk_001';

    /**
     * api列表
     * @var array
     * @example
     *  [
     *      // get为方法名
     *      'get' => [
     *          // method必填，指定请求方式
     *          'method' => 'get',
     *          // url选填，不填的时候则默认为当前包名+类名+方法名，自动把驼峰命名转化为下划线
     *          'url' => 'user/address/get',
     *      ],
     *  ]
     */
    protected $function = [];

    /**
     * 使用递归处理参数生成加密字符串时暂存字符串的变量
     * @var string
     * @datetime 2019/5/24 12:49
     * @author jianghongjie
     */
    protected $signStr = '';

    /**
     * 最后一次错误信息
     * @var array
     */
    protected $errorInfo = [];

    /**
     * @param $name
     * @param $arguments
     * @throws \Exception
     * @return array
     */
    public function __call($name, $arguments)
    {
        try {
            $url = $this->getUrl($name);
            $method = $this->getMethod($name);

            if (isset($arguments[0])) {
                $param = $arguments[0];
            } else {
                throw new \Exception('Missing Parameters');
            }
            // 签名
            $param['sign'] = $this->getSign($param);

            $result = $this->requestApi($url, $method, $param);

        } catch (\Exception $e) {
            $result = Result::Error($e->getMessage(), self::CODE_SDK_EXCEPTION);
        }
        return $result->toCallback();
    }

    /**
     * 请求api
     * @author PengShuHai<pengshuhai@jybdshop.cn>
     * @date 2019/2/1 15:48:00
     * @param string $url  url
     * @param string $method get/post/...
     * @param array $param param
     * @return Result
     * @throws \Exception
     */
    protected function requestApi( $url,$method, $param )
    {
        $response = HttpCurl::request($url, $method, $param, [], 30);
        if ($response->state) {
            if ($response->httpCode == 200) {
                $data = json_decode($response->body, true);
                $result = isset($data['result']) ? $data['result'] : [];
                $code = isset($data['code']) ? $data['code'] : '';
                $msg = isset($data['msg']) ? $data['msg'] : '';
                if ($code == self::CODE_API_SUCCESS) {
                    return Result::Success($result, $msg);
                } else {
                    if ($code === '') {
                        $msg = '服务处理失败，请稍后再试';
                        $code = self::CODE_API_SERVER_ERROR;
                    }
                    $this->setLastError($response);
                    return Result::Error($msg, $code);
                }
            } else {
                $msg = '服务处理失败，请稍后再试';
                $this->setLastError($response);
                return Result::Error($msg, self::CODE_API_SERVER_ERROR);
            }
        } else {
            $this->setLastError($response);
            throw new \Exception('curl error: ' . $response->error);
        }
    }

    /**
     * 获取http url
     * @author PengShuHai<pengshuhai@jybdshop.cn>
     * @date 2019/1/31 23:12:55
     * @param $funcName
     * @return string url
     * @throws \Exception
     */
    protected function getUrl($funcName)
    {
        if (isset($this->function[$funcName])) {
            $info = $this->function[$funcName];
            if (isset($info['url']) && !empty($info['url'])) {
                $url = $info['url'];
            } else {
                $namespace = get_class($this);
                $namespaceArr = explode('\\', $namespace);

                $className = $this->humpToUnderline(array_pop($namespaceArr));
                $packageName = $this->humpToUnderline(array_pop($namespaceArr));
                $func = $this->humpToUnderline($funcName);

                $url = $packageName.'/'.$className.'/'.$func;
            }
            $doMain = Config::get('doMain');
            if (empty($doMain)) {
                throw new \Exception('domain does not exist');
            }
            return rtrim($doMain, '/').'/'.ltrim($url, '/');
        } else {
            throw new \Exception("function '".$funcName."' does not exist");
        }
    }

    /**
     * 获取http请求方式
     * @author PengShuHai<pengshuhai@jybdshop.cn>
     * @date 2019/1/31 23:14:25
     * @param $funcName
     * @return string method
     * @throws \Exception
     */
    protected function getMethod($funcName)
    {
        if (isset($this->function[$funcName])) {
            $info = $this->function[$funcName];
            if (isset($info['method'])) {
                return $info['method'];
            } else {
                return '';
            }
        } else {
            throw new \Exception("function '".$funcName."' does not exist");
        }
    }


    /**
     * 驼峰转下划线
     * @author PengShuHai<pengshuhai@jybdshop.cn>
     * @date 2019/1/31 23:27:17
     * @param $name
     * @return string
     */
    protected function humpToUnderline($name)
    {
        $name = StringHelpers::Camel2words($name, false);
        return str_replace(' ', '_', $name);
    }

    /**
     * 获取签名
     * @author PengShuHai<pengshuhai@jybdshop.cn>
     * @date 2019/1/17 14:33:23
     * @param array $param 参数
     * @return string
     */
    protected function getSign(array $param){
        // 添加key参数
        $param['key'] = Config::get('signKey');

        // 键从小到大排序
        ksort($param);
        reset($param);

        // 值拼接起来, md5
        $str = $this->multiArrayToString($param);
        $this->signStr = '';
        return md5($str);
    }

    /**
     * 多维数组拼接成字符串
     * @param $array
     * @return string
     * @date   2019/5/23 21:40
     * @author jianghongjie
     */
    protected function multiArrayToString($array)
    {
        foreach ($array as $value) {
            if (is_array($value)) {
                $this->multiArrayToString($value);
            } else {
                $this->signStr .= $value;
            }
        }
        return $this->signStr;
    }

    /**
     * 设置最后一次错误信息
     * @author PengShuHai<pengshuhai@jybdshop.cn>
     * @date 2019/6/13 10:57:03
     * @param HttpCurlResponse $response
     */
    protected function setLastError(HttpCurlResponse $response)
    {
        $this->errorInfo = json_decode(json_encode($response), true);
    }

    /**
     * 获取最后一次错误信息
     * @author PengShuHai<pengshuhai@jybdshop.cn>
     * @date 2019/6/13 10:57:34
     * @return array
     */
    public function getLastError()
    {
        return $this->errorInfo;
    }

}