<?php
/**
 * Created by PhpStorm.
 * User: lihao
 * Date: 2019-07-02
 * Time: 17:58
 */

namespace promoter\api\common;

use promoter\api\Api;

/**
 * Class Group
 * @package promoter\api\common;
 *
 * @method array add(array $param); 添加任务
 * @method array update(array $param);修改任务信息
 * @method array get(array $param); 查看任务信息
 */
class Task extends Api
{
    protected $function = [
        'add' => [
            'method' => 'post'
        ],
        'update' => [
            'method' => 'post'
        ],
        'get' => [
            'method' => 'get'
        ],

    ];
}