<?php
/**
 * Created by PhpStorm.
 * User: lihao
 * Date: 2019-07-02
 * Time: 17:39
 */

namespace promoter\api\redpacket;


use promoter\api\Api;

/**
 * Class Group
 * @package promoter\api\redpacket
 *
 * @method array add(array $param); 添加红包
 * @method array update(array $param);修改红包信息
 * @method array get(array $param); 查看红包信息
 * @method array list(array $param); 红包信息列表
 * @method array count(array $param); 红包信息总数
 * @method array asynchronous_receive_redpacket(array $param); 异步领取红包
 * @method array synchronous_receive_redpacket(array $param); 同步领取红包
 * @method array certification_up_redpacket(array $param); 认证红包生效
 */
class Redpacket extends Api
{
    protected $function = [
        'add' => [
            'method' => 'post'
        ],
        'update' => [
            'method' => 'post'
        ],
        'get' => [
            'method' => 'get'
        ],
        'list' => [
            'method' => 'get'
        ],
        'count' => [
            'method' => 'get'
        ],
        'asynchronousReceiveRedpacket' => [
            'method' => 'get'
        ],
        'synchronousReceiveRedpacket' => [
            'method' => 'get'
        ],
        'certificationUpRedpacket' => [
            'method' => 'get'
        ],

    ];
}