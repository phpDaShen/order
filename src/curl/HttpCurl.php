<?php

namespace promoter\curl;

/**
 * HttpCurl Curl模拟Http工具类
 * Class HttpCurl
 * @package core\curl
 */
class HttpCurl {

    /**
     * 模拟POST/GET/PUT/DELETE/PATCH请求
     *
     * Examples:
     * ```
     * HttpCurl::request('http://example.com/', 'post', array(
     *  'user_uid' => 'root',
     *  'user_pwd' => '123456'
     * ));
     *
     * HttpCurl::request('http://example.com/', 'post', '{"name": "peter"}');
     *
     * HttpCurl::request('http://example.com/', 'post', array(
     *  'file1' => '@/data/sky.jpg',
     *  'file2' => '@/data/bird.jpg'
     * ));
     *
     * // windows
     * HttpCurl::request('http://example.com/', 'post', array(
     *  'file1' => '@G:\wamp\www\data\1.jpg',
     *  'file2' => '@G:\wamp\www\data\2.jpg'
     * ));
     *
     * HttpCurl::request('http://example.com/', 'get');
     *
     * HttpCurl::request('http://example.com/?a=123', 'get', array('b'=>456));
     * ```
     *
     * @param string $url [请求地址]
     * @param string $type [请求方式 post or get]
     * @param bool|string|array $data [传递的参数]
     * @param array $header [可选：请求头] eg: ['Content-Type:application/json']
     * @param int $timeout [可选：超时时间]
     *
     * @return HttpCurlResponse
     */
    public static function request($url, $type, $data = false, $header = [], $timeout = 0) 
    {
        $cl = curl_init();
        // 兼容HTTPS
        if (stripos($url, 'https://') !== FALSE) {
            curl_setopt($cl, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($cl, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($cl, CURLOPT_SSLVERSION, 1);
        }
        // 设置返回内容做变量存储
        curl_setopt($cl, CURLOPT_RETURNTRANSFER, 1);
        // 设置需要返回Header
        curl_setopt($cl, CURLOPT_HEADER, true);
        // 设置请求头
        if (count($header) > 0) {
            curl_setopt($cl, CURLOPT_HTTPHEADER, $header);
        }
        // 设置需要返回Body
        curl_setopt($cl, CURLOPT_NOBODY, 0);
        // 设置超时时间
        if ($timeout > 0) {
            curl_setopt($cl, CURLOPT_TIMEOUT, $timeout);
        }
        // 请求参数处理
        $type = strtoupper($type);
        switch ($type) {
            case 'GET':
                if (is_array($data)) {
                    if (stripos($url, "?") === FALSE) {
                        $url .= '?';
                    }
                    $url .= http_build_query($data);
                }
                break;

            case 'POST':
                curl_setopt($cl, CURLOPT_POST, true);
                curl_setopt($cl, CURLOPT_POSTFIELDS, http_build_query($data));
                break;

            case 'PUT':
                curl_setopt($cl, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($cl, CURLOPT_POSTFIELDS, $data);
                break;

            case 'DELETE':
                curl_setopt($cl, CURLOPT_CUSTOMREQUEST, "DELETE");
                curl_setopt($cl, CURLOPT_POSTFIELDS, $data);
                break;

            case 'PATCH':
                curl_setopt($cl, CURLOPT_CUSTOMREQUEST, "PATCH");
                curl_setopt($cl, CURLOPT_POSTFIELDS, $data);
                break;
        }

        curl_setopt($cl, CURLOPT_URL, $url);
        // 读取获取内容
        $response = curl_exec($cl);

        $httpCurlResponse = new HttpCurlResponse($response,$cl);

        // 关闭Curl
        curl_close($cl);

        return $httpCurlResponse;
    }


    /**
     * get请求
     * @author PengShuHai<pengshuhai@jybdshop.cn>
     * @date 2019/1/17 13:40:10
     * @param string $url url
     * @param bool|string|array $data  数据参数
     * @param array $header 头信息
     * @param int $timeout 响应超时
     * @return string
     */
    public static function get(string $url, $data = false, $header = [], $timeout = 0)
    {
        $httpCurlResponse = static::request($url, 'GET', $data, $header, $timeout);
        return $httpCurlResponse->body;
    }


    /**
     * post请求
     * @author PengShuHai<pengshuhai@jybdshop.cn>
     * @date 2019/1/17 13:40:10
     * @param string $url url
     * @param bool|string|array $data  数据参数
     * @param array $header 头信息
     * @param int $timeout 响应超时
     * @return string
     */
    public static function post(string $url, $data = false, $header = [], $timeout = 0)
    {
        $httpCurlResponse = static::request($url, 'POST', $data, $header, $timeout);
        return $httpCurlResponse->body;
    }


    /**
     * put请求
     * @author PengShuHai<pengshuhai@jybdshop.cn>
     * @date 2019/1/17 13:40:10
     * @param string $url url
     * @param bool|string|array $data  数据参数
     * @param array $header 头信息
     * @param int $timeout 响应超时
     * @return string
     */
    public static function put(string $url, $data = false, $header = [], $timeout = 0)
    {
        $httpCurlResponse = static::request($url, 'PUT', $data, $header, $timeout);
        return $httpCurlResponse->body;
    }

    /**
     * delete请求
     * @author PengShuHai<pengshuhai@jybdshop.cn>
     * @date 2019/1/17 13:40:10
     * @param string $url url
     * @param bool|string|array $data  数据参数
     * @param array $header 头信息
     * @param int $timeout 响应超时
     * @return string
     */
    public static function delete(string $url, $data = false, $header = [], $timeout = 0)
    {
        $httpCurlResponse = static::request($url, 'DELETE', $data, $header, $timeout);
        return $httpCurlResponse->body;
    }


    /**
     * patch请求
     * @author PengShuHai<pengshuhai@jybdshop.cn>
     * @date 2019/1/17 13:40:10
     * @param string $url url
     * @param bool|string|array $data  数据参数
     * @param array $header 头信息
     * @param int $timeout 响应超时
     * @return string
     */
    public static function patch(string $url, $data = false, $header = [], $timeout = 0)
    {
        $httpCurlResponse = static::request($url, 'PATCH', $data, $header, $timeout);
        return $httpCurlResponse->body;
    }
}
