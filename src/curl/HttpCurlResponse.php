<?php

namespace promoter\curl;

/**
 * curl响应数据集
 * Class HttpCurlResponse
 * @package core\curl
 */
class HttpCurlResponse
{
    /**
     * 请求是否正确
     * @var bool
     */
    public $state;

    /**
     * http状态码
     * @var int
     */
    public $httpCode = 0;

    /**
     * 响应正文
     * @var string
     */
    public $body = '';

    /**
     * 响应头
     * @var string
     */
    public $header = '';

    /**
     * 响应状态
     * @var array
     */
    public $status = [];

    /**
     * 错误码
     * @var int
     */
    public $errno = 0;

    /**
     * 错误描述
     * @var string
     */
    public $error = '';


    /**
     * HttpCurlResponse constructor.
     * @param string $response curl_exec的返回内容
     * @param resource $handle curl句柄
     */
    public function __construct( $response, $handle)
    {
        // 读取获取内容
        $this->body = $response;
        // 读取状态
        $this->status = curl_getinfo($handle);
        // 读取错误号
        $this->errno = curl_errno($handle);
        // 读取错误详情
        $this->error = curl_error($handle);

        if (isset($this->status['http_code'])) {
            $this->httpCode = $this->status['http_code'];
        }

        if ($this->check()) {
            $this->state = true;
            $this->header = substr($response, 0, $this->status['header_size']);
            $this->body = substr($this->body, $this->status['header_size']);
        } else {
            $this->state = false;
            $this->header = '';
            $this->body = '';
        }
    }


    /**
     * 验证响应是否正确
     * @author PengShuHai<pengshuhai@jybdshop.cn>
     * @date 2019/1/17 13:09:48
     * @return bool
     */
    public function check()
    {
        if ($this->errno == 0 && isset($this->status['http_code'])) {
            return true;
        } else {
            return false;
        }
    }

}